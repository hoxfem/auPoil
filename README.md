# Au poil !

* Ensemble de routines permettant de compter le nombre de poils sur une image
* Développé dans le cadre des clubs science / [MATh.en.JEANS](https://fr.wikipedia.org/wiki/MATh.en.JEANS) du [collège Paul Langevin](https://plangevin-coueron.loire-atlantique.e-lyco.fr/), Couëron (2021-2022)
