#!/usr/bin/env python


from asyncio.constants import DEBUG_STACK_DEPTH
import numpy as np
import matplotlib.pyplot as plt

from skimage import data, color
from skimage.transform import hough_circle, hough_circle_peaks, hough_line, hough_line_peaks
from skimage.feature import canny
from skimage.draw import circle_perimeter
from skimage.util import img_as_ubyte
import skimage as ski
import skimage.io

import sys

# plt.ion()




decalage = np.array([[0,1], [0,-1], [-1,0], [1,0], [-1,1], [1,-1], [-1,-1], [1,1]])


def recuperer_voisins_noirs_recursive(racine, img, voisins_OK_):

    voisins_OK = []
    shapeimg = img.shape

    for dec in decalage:
        position_voisin = racine + dec


        # print('voisin', position_voisin)

        # On corrige la position
        if position_voisin[0] < 0 or position_voisin[0] >= shapeimg[0] or position_voisin[1] < 0 or position_voisin[1] >= shapeimg[1]:
            continue
        
        valeur_pixel = img[position_voisin[0], position_voisin[1]]
        
        if valeur_pixel == False:
            voisins_OK.append(position_voisin)
            img[position_voisin[0], position_voisin[1]] = True
            recuperer_voisins_noirs_recursive(position_voisin, img, voisins_OK)

    if len(voisins_OK):
        voisins_OK_ += voisins_OK
        return True
    else :
        return False





def main(filename, thresh, filter=False):



    # Load picture and detect edges
    # image = ski.io.imread('test_manuel.jpg'); thresh = 100
    # image = ski.io.imread('img1.png'); thresh = 150
    image = ski.io.imread(filename)

    

    if len(image.shape) == 3 :
        image = img_as_ubyte(ski.color.rgb2gray(image))

    # image = img_as_ubyte(data.coins()[160:230, 70:270])

    # image = np.ones((9,9),bool)
    # image[0:4,2] = False
    # image[1,1:4] = False

    # image[6,5:8] = False
    # image[5:8,6] = False

    if not filter:
        lenMin = -1
    else:
        lenMin = 1


    plt.figure(1)
    plt.imshow(image,cmap=plt.cm.gray)
    plt.title('Image d\'entrée')
    
    threshold = image < thresh

    # if np.min(image.shape) > 40:
        # threshold = threshold[5:-5, 5:-5]

    threshold = np.logical_not(threshold)


    # threshold = image.copy()

    plt.figure(2)
    plt.imshow(threshold,cmap=plt.cm.gray)
    plt.title('Image seuillée')

    ilots = []

    k = 0
    while np.logical_not(threshold.min()):

        racine = np.argwhere(threshold==False)[0]

        ilot = [racine]
        threshold[racine[0], racine[1]] = True

        
        while True:
            termine = recuperer_voisins_noirs_recursive(racine, threshold, ilot)

            if termine == False:
                break
        

        if len(ilot) > lenMin:
            ilots.append(ilot)
            k += 1

        # bbbb


    # Trace des ilots
    imgSeg = np.zeros(image.shape)
    values = np.arange(len(ilots))

    plt.figure(3)
    for k, ilot in enumerate(ilots):
        # print('pp', len(ilot))
        for loc in ilot:
            imgSeg[loc[0], loc[1]] = k+1

    plt.imshow(imgSeg)
    plt.colorbar()
    plt.title('Image segmentée : ' + str(len(ilots)) + ' ilot(s)')

    plt.show()

    print('out of main')




if __name__ == '__main__':

    print('fichier', sys.argv[1])
    print('thresh', sys.argv[2])
    main(sys.argv[1], int(sys.argv[2]))
