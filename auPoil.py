#!/usr/bin/env python
 
# simple.py

import wx
import recursiveFunc as rf


# def OnQuit(app, e):
#         app.Close()


# app = wx.App()

# frame = wx.Frame(None, title='Au poil !')

# menubar = wx.MenuBar()
# fileMenu = wx.Menu()
# fileItem = fileMenu.Append(wx.ID_EXIT, 'Quit', 'Quit application')
# menubar.Append(fileMenu, '&File')
# frame.SetMenuBar(menubar)
# frame.Bind(wx.EVT_MENU, OnQuit, fileItem)

# frame.Show()

# app.MainLoop()



class Example(wx.Frame):

    def __init__(self, *args, **kwargs):
        super(Example, self).__init__(*args, **kwargs)

       
        self.filePath = ''
        self.thresh = 0
        self.openFileDialog = None
        self.textzone = None
        self.thresh_value = None
        self.filterUniquePixelsBox = None
        
        self.InitUI()


    def InitUI(self):

        # menubar = wx.MenuBar()
        # fileMenu = wx.Menu()
        # fileItem = fileMenu.Append(wx.ID_EXIT, 'Quitter', 'Quitter l\'application')
        # openItem = fileMenu.Append(wx.ID_OPEN, '&Ouvrir')
        self.textzone = wx.StaticText(self,wx.ID_ANY, 'Image courante : AUCUNE', pos=(5, 110), size = (300,20)) 

        self.thresh_value = wx.TextCtrl(self, value='150', size=(100, 30), pos=(130, 5))
        openbtn = wx.Button(self, label='Ouvrir image', pos=(5, 40))
        runbtn = wx.Button(self, label='Lancer', pos=(130, 40))
        quitbtn = wx.Button(self, label='Quitter', pos=(225, 40))

        self.filterUniquePixelsBox = wx.CheckBox(self, label = "Filtrer les ilots ?", pos=(5, 80))



        # menubar.Append(fileMenu, '&Fichier')
        # self.SetMenuBar(menubar)

        # self.Bind(wx.EVT_MENU, self.OnQuit, fileItem)
        # self.Bind(wx.EVT_MENU, self.OpenDialog, openItem)
        openbtn.Bind(wx.EVT_BUTTON, self.OpenDialog)
        quitbtn.Bind(wx.EVT_BUTTON, self.OnQuit)
        runbtn.Bind(wx.EVT_BUTTON, self.RUN)
        self.thresh_value.Bind(wx.EVT_TEXT_ENTER, self.UpdateThreshold)

        

        

        self.SetSize((325, 200))
        self.SetTitle('Au poil !')
        self.Centre()

    def OnQuit(self, e):
        print(self.filePath)
        self.Close()
    
    def updateText(self, txt):
        # self.textzone.SetLabel(txt)
        pass

    def UpdateThreshold(self, e):
        print('refresh',self.thresh_value.GetValue())


    def OpenDialog(self, e):
        self.openFileDialog = wx.FileDialog(self, "Open", "", "","Images (*.png,*.jpg,*.jpeg,*.tiff)|*.png;*.jpg;*.jpeg;*.tiff",wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        self.openFileDialog.ShowModal()
        # print(openFileDialog.GetPath())
        self.filePath = self.openFileDialog.GetPath()
        self.textzone.SetLabel('Image courante : ...' + self.openFileDialog.GetPath()[-25:])
        # self.textzone.Wrap(300)
        # self.updateText(self.openFileDialog.GetPath())

    def RUN(self, e):

        if not self.filePath or not self.thresh:
            print('Il faut définir une image et un seuil !')

        # path = self.filePath

        path = self.filePath
        num = int(self.thresh_value.GetValue())
        # print('bbb', self.thresh_value.GetValue())
        doFiltering = self.filterUniquePixelsBox.GetValue()

        print('RUN called with', path, num, doFiltering)
        

        rf.main(path, num, doFiltering)
    
    
        






def main():

    app = wx.App()
    ex = Example(None)
    ex.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()